---
layout: 2020/post
section: propuestas
category: devrooms
title: Cultura Libre
---

La cultura y el conocimiento libres sustentan la idea de bienes comunes del conocimiento y el derecho de todas las personas a participar en la vida cultural. Desde Wikimedia España defendemos los proyectos Wikimedia y también los del resto de agentes de cultura libre como bienes comunes dentro de una Internet libre y abierta.

El propósito de esta sala es generar un debate y punto de encuentro dentro de las comunidades libres para celebrar y conocer distintos proyectos, tecnologías y metodologías. También es nuestro objetivo explorar nuevas vías para hacer juntas y para defender el derecho a compartir y construir conocimiento y cultura en línea.

Nos gustaría que en la sala se muestren diversos modelos y proyectos de cultura libre. Sobre cine, literatura, fotografía, periodismo, festivales, proyectos Wikimedia. Estos pueden ser independientes, activistas, privados, públicos o en colaboración con instituciones.

Se pondrá el foco en revisar y debatir políticas públicas que impulsen y protejan la cultura y el conocimiento libre en España y Europa, buscando medidas que no pongan en peligro el acceso al conocimiento y la cultura en línea, y que protejan a quienes participan en su construcción colectiva.

## Comunidad o grupo que lo propone

**Wikimedia España** es el capítulo de la Fundación Wikimedia en el Estado español. Somos una asociación sin ánimo de lucro que promueve el conocimiento libre y los proyectos Wikimedia, siendo Wikipedia el más conocido. Nos constituimos en 2011 y llevamos todos estos años trabajando para mejorar la accesibilidad, el uso y la participación en Wikipedia y sus proyectos hermanos tanto a nivel social como institucional.

La visión del movimiento Wikimedia es conseguir un mundo en el que todas las personas tengan acceso libre a la suma del conocimiento y puedan participar en su construcción colectiva. Seguimos las filosofías del conocimiento y la cultura libres, que defienden el derecho fundamental de acceso a estos, y todos nuestros proyectos operan sobre plataformas de software libre.

Wikipedia, la enciclopedia que todo el mundo puede editar, es nuestro proyecto más conocido, pero trabajamos con otros 12 proyectos colaborativos de conocimiento libre en la red. Por ejemplo, Wikidata, una base de datos estructurada; o Wikimedia Commons, el repositorio multimedia libre más grande del mundo.

Todo el mundo tiene conocimiento para compartir. Desde escribir o mejorar un artículo en Wikipedia, a documentar manifestaciones culturales y subir las imágenes a Wikimedia Commons. Personas individuales, colectivos u organizaciones: todas pueden formar parte del ecosistema Wikimedia.

### Contactos

-   **Virginia Díez**: virginiadiez at wikimedia dot es
-   **Florencia Claes**: florenciaclaes at wikimedia dot es

## Público objetivo

A todo el mundo, aunque el objetivo es lograr una conexión entre agentes culturales, instituciones, creadores y la comunidad del software libre.

## Tiempo

Media jornada (mañana o tarde).

## Día

5 y 6 de junio.

## Formato

Se propone una devroom multiformato que se programará junto a ponentes y participantes en una fase posterior si la propuesta es aceptada.

De ser así se convocará participación para:

-   Charlas relámpago y tradicionales en las que se presenten iniciativas de cultura libre (archivos y bibliotecas, editoriales, cine y contenidos audiovisuales, música, etc)
-   Mesas redondas sobre temas que atraviesan la cultura libre como la regulación de derechos de autoría, la desinformación o la brecha de género
-   Charlas plenarias sobre derechos digitales
-   Cine-forum con proyección y mesa de debate

Pensamos que diversas manifestaciones de cultura libre se podrían mostrar en esLibre, en esta u otras salas. Dejamos constancia de nuestra disponibilidad e ilusión para llevarlas a cabo en colaboración con otras organizaciones/ponentes.

## Comentarios

Si se acepta la propuesta la promocionaremos a través de nuestra web.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
-   [x] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
